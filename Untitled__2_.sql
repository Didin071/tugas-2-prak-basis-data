CREATE TABLE `Aplikasi_KAI` (
  `id_kereta` integer PRIMARY KEY,
  `tujuan` varchar(255),
  `nama_kereta` varchar(255)
);

CREATE TABLE `Tujuan` (
  `id_kereta` integer,
  `nama_cs` varchar(255),
  `waktu_berangkat` timestamp,
  `waktu_tiba` datetime
);

CREATE TABLE `pembayaran` (
  `id_kereta` integer,
  `nama_kereta` varchar(255),
  `harga_kereta` integer,
  `jumlah_nominal` integer
);

ALTER TABLE `Tujuan` ADD FOREIGN KEY (`id_kereta`) REFERENCES `Aplikasi_KAI` (`id_kereta`);

ALTER TABLE `pembayaran` ADD FOREIGN KEY (`id_kereta`) REFERENCES `Tujuan` (`id_kereta`);
